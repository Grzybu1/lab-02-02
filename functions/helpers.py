from datetime import date

def print_header():
    '''Function printing current system date and welcome text'''
    print('Welcome to lab02 by Motorola Solutions')
    print(f'Today is: {date.today()}')

def hello_student(name:str):
    '''Function printing greeting to the person given as parameter'''
    print(f'Hello {name}')